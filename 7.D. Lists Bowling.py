# --autora-- = "Valeria Guerrero"
# --email-- = "valeria.guerrero@unl.edu.ec"

#Ejercicio 13:
            #En los bolos, el jugador comienza con 10 pines seguidos en el extremo más
            #alejado de un carril. El objetivo es derribar todos los pines. Para esta
            #tarea, el número de alfileres y bolas variará. Dado el número de pines N y
            #luego el número de bolas K a rodar, seguido de K pares de números (uno para
            #cada bola rodada), determine qué pines permanecen en pie después de que se hayan
            #rodado todas las bolas.
            #Las bolas están numeradas del 1 al N para esta situación. Los pares de números
            #subsiguientes, uno para cada K, representan la primera y última posición (inclusive)
            #de los pasadores que fueron derribados con cada rollo. Imprima una secuencia de N
            #caracteres, donde "I" representa un pin dejado en pie y "." representa un alfiler derribado."""
# Read a list of integers:
# a = [int(s) for s in input().split()]
# Print a value:
# print(a)
n, k = [int(s) for s in input().split()]
pines = ['I'] * n
for i in range(k):
  izq, derecha = [int(s) for s in input().split()]
  for j in range(izq - 1, derecha):
    pines[j] = '.'
print(''.join(pines))
