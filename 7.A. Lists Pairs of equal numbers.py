# --autora-- = "Valeria Guerrero"
# --email-- = "valeria.guerrero@unl.edu.ec"

#Ejercicio 10:
            #Dada una lista de números, cuente varios pares de elementos iguales.
            #Cualquier dos elementos que sean iguales entre sí deben contarse
            #exactamente una vez."""
# Read a list of integers:
# a = [int(s) for s in input().split()]
# Print a value:
# print(a)
a = [int(s) for s in input().split()]
contador = 0
for num in range(len(a)):
  for num2 in range(num + 1, len(a)):
    if a[num] == a[num2]:
      contador+=1
print(contador)