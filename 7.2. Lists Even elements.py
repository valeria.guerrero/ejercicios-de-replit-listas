# --autora-- = "Valeria Guerrero"
# --email-- = "valeria.guerrero@unl.edu.ec"

#Ejercicio 2:
            #Dada una lista de números, imprima todos sus elementos pares. Use un bucle
            #for que itera sobre la lista misma y no sobre sus índices. Es decir, no use
            #range ()"""
# Read a list of integers:
# a = [int(s) for s in input().split()]
#  print (a)
# print(a)
a = input().split()
for i in range(len(a)):
    a[i] = int(a[i])
    if a[i] %2==0:
        print(a[i])
