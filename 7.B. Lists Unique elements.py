# --autora-- = "Valeria Guerrero"
# --email-- = "valeria.guerrero@unl.edu.ec"

#Ejercicio 11:
            #Dada una lista de números, busque e imprima los elementos que aparecen en
            #ella solo una vez. Dichos elementos deben imprimirse en el orden en que
            #aparecen en la lista original."""


# Read a list of integers:
# a = [int(s) for s in input().split()]
# Print a value:
# print(a)
a = [int(s) for s in input().split()]
for i in range(len(a)):
    for j in range(len(a)):
        if i != j and a[i] == a[j]:
            break
        else:
            print(a[i], end=' ')
