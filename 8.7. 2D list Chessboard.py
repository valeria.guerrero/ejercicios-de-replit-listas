# --autora-- = "Valeria Guerrero"
# --email-- = "valeria.guerrero@unl.edu.ec"

#Ejercicio 20:
            #Dados dos enteros positivos n y m , cree una matriz bidimensional de tamaño
            #n × m y llénela con los caracteres "." y "*" en un patrón a cuadros. La esquina
            #superior izquierda debe tener el carácter "."."""

# Read a list of integers:
# a = [int(s) for s in input().split()]
# Print a value:
# print(a)
n, m = [int(s) for s in input().split()]
a = [['.' if (i + j) % 2 == 0 else '*']
     for i in range(n)
     for j in range(m)]
for fil_col in a:
    print(*fil_col)
