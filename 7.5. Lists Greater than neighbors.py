# --autora-- = "Valeria Guerrero"
# --email-- = "valeria.guerrero@unl.edu.ec"

#Ejercicio 5:
            #Dada una lista de números, determine e imprima el número de elementos que
            #son mayores que sus dos vecinos.
            #El primero y el último elemento de la lista no deben considerarse porque no
            #tienen dos vecinos."""
# Read a list of integers:
# a = [int(s) for s in input().split()]
# Print a value:
# print(a)
a = [int(s) for s in input().split()]
# Print a value:
# print(a)
elementos = 0
for i in range(1, len(a) - 1):
  if a[i - 1] < a[i] > a[i + 1]:
    elementos += 1
print(elementos)
