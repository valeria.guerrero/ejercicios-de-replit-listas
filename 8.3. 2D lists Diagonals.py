# --autora-- = "Valeria Guerrero"
# --email-- = "valeria.guerrero@unl.edu.ec"

#Ejercicio 16:
            #Dado un número entero n , cree una matriz bidimensional de tamaño n × n de
            #acuerdo con las siguientes reglas e imprímalo:
            #En la diagonal principal ponga 0.
            #En las diagonales adyacentes a la principal pon 1.
            #En las siguientes diagonales adyacentes ponga 2 , y así sucesivamente."""

# Read an integer:
n = int(input())
entrada = [i for i in range(n)]
matriz = [[]]*n
for j in range(n):
  matriz[j] = entrada[j:0:-1]
  for l in entrada[:n-j]:
    matriz[j].append(l)
  print(*matriz[j])