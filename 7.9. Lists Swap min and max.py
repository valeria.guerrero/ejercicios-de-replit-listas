# --autora-- = "Valeria Guerrero"
# --email-- = "valeria.guerrero@unl.edu.ec"

#Ejercicio 9:
            #Dada una lista de números distintos, intercambie el mínimo y el máximo e
            #imprima la lista resultante."""
# Read a list of integers:
# a = [int(s) for s in input().split()]
# Print a value:
# print(a)
a = [int(s) for s in input().split()]

max, min = a.index(max(a)), a.index(min(a))
a[max], a[min] = a[min], a[max]

print(a)
