# --autora-- = "Valeria Guerrero"
# --email-- = "valeria.guerrero@unl.edu.ec"

#Ejercicio 8:
            #Dada una lista de enteros, encuentre el primer elemento máximo en él.
            #Imprime su valor y su índice (contando con 0)."""

# Read a list of integers:
# a = [int(s) for s in input().split()]
# Print a value:
# print(a)
a = [int(s) for s in input().split()]
maximo = max(a)
print(maximo, a.index(maximo))