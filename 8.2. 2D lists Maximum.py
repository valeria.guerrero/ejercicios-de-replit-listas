# --autora-- = "Valeria Guerrero"
# --email-- = "valeria.guerrero@unl.edu.ec"

#Ejercicio 15:
            #Dados dos enteros, el número de filas my columnas n de la lista m × n 2d, y
            #las siguientes filas m de n enteros, encuentre el elemento máximo e imprima
            #su número de fila y número de columna. Si hay muchos elementos máximos en
            #diferentes filas, informe el que tenga un número de fila más pequeño. Si hay
            #muchos elementos máximos en la misma fila, informe el que tenga un número de
            #columna más pequeño."""

# Read a 2D list of integers:
# a = [[int(j) for j in input().split()] for i in range(NUM_ROWS)]
# Print a value:
# print(a)
m, n = [int(s) for s in input().split()]
for i in range(0, m):
    listamn = list(map(int, input().split()))
    if i == 0:
        max_el = max(listamn)
        maxLista = [0, listamn.index(max(listamn))]
    elif max(listamn) > max_el:
        max_el = max(listamn)
        maxLista = [i, listamn.index(max(listamn))]
print(*maxLista, sep=' ')