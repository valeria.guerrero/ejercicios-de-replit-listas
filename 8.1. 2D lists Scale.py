# --autora-- = "Valeria Guerrero"
# --email-- = "valeria.guerrero@unl.edu.ec"

#Ejercicio 14:
            #Given two integers - the number of rows m and columns n of m×n 2d list - and
            #subsequent m rows of n integers, followed by one integer c. Multiply every
            #element by c and print the result."""

# Read a 2D list of integers:
d = input().split()
NUM_ROWS= int(d[0])
NUMS_COLS= int(d[1])
a = [[int(j) for j in input().split()] for i in range(NUM_ROWS)]

c = int(input())
for cont_i in range(NUM_ROWS):
  for cont_j in range(NUMS_COLS):
    a[cont_i] [cont_j] = a[cont_i][cont_j]*c
    
for fila in a:
  for columna in fila:
    print(columna, end= " ")
  print()
