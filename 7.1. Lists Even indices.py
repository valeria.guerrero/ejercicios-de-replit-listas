# --autora-- = "Valeria Guerrero"
# --email-- = "valeria.guerrero@unl.edu.ec"

#Ejercicio 1:
            #Dada una lista de números, encuentre e imprima todos sus elementos con índices
            #pares (es decir, A [0] , A [2] , A [4] , ...)."""


# Read a list of integers:
a = [int(s) for s in input().split()]
# Print a value:
#print(a)
indice = 0
for i in a: 
  if indice %2 == 0:
    print (i)
  indice+=1